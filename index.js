
const last = (list) => list[list.length - 1];

try {

  const args = process.argv;
  if (args.length !== 3) {
    throw "Pass brackets as an argument. Eg: '{[{[]}[]]}'";
  }
  const brackets = args[2].split('');
  const openingBrackets = ['{', '[', '('];
  const closingBrackets = ['}', ']', ')'];
  const bracketRef = {
    '{': '}',
    '[': ']',
    '(': ')'
  }

  const stack = new Array();

  brackets.forEach(item => {
    if (stack.length === 0) {
      stack.push(item);
      return;
    }
    const isClosingBracket = closingBrackets.includes(item);
    const isOpeningBracket = openingBrackets.includes(item);

    if(!isClosingBracket && !isOpeningBracket) {
      throw `Unknown character detected => ${item} at char ${brackets.indexOf(item)}. It has to be one of '{}[]()'`;
    }
  
    if (isClosingBracket) {
      const lastElement = last(stack);
      if (bracketRef[lastElement] === item) {
        stack.pop();
      }
    }
    if (isOpeningBracket) {
      stack.push(item);
    }
  });

  if (stack.length === 0) {
    console.log('Balanced');
  } else {
    console.log('Not balanced');
  }
}
catch (e) {
  console.log(e);
}